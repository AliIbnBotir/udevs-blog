// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getFirestore } from "@firebase/firestore";
import { getAuth } from "firebase/auth";
import { getStorage } from "firebase/storage";

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyCR3tngnh0NwJNZfnH_1C59CLuIu5Ym9iE",
  authDomain: "udevs-915a0.firebaseapp.com",
  databaseURL: "https://udevs-915a0-default-rtdb.firebaseio.com",
  projectId: "udevs-915a0",
  storageBucket: "udevs-915a0.appspot.com",
  messagingSenderId: "509655646495",
  appId: "1:509655646495:web:905eba542fcfdc6dcab02a",
  measurementId: "${config.measurementId}",
};

// Initialize Firebase
export const app = initializeApp(firebaseConfig);
export const db = getFirestore();
export const auth = getAuth();
export const storage = getStorage(app);

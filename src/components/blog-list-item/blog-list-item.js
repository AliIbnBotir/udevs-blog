import React from "react";
import Eye from "./eye.svg";
import { Link } from "react-router-dom";
import "./blog-list-item.css";

function BlogListItem({ item }) {
  return (
    <Link to={`/${item.id}`}>
      <div className="blog-list--item">
        <img src={item.image} alt="fotka" className="post-image rounded" />
        <p className="secondary-info">
          18:26 11.01.2021 |
          <span>
            <img src={Eye} alt="" />
          </span>{" "}
          365
        </p>
        <p className="blog-post">{item.title} .....</p>
      </div>
    </Link>
  );
}

export default BlogListItem;

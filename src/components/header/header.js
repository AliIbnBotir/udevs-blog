import React from "react";
import logo from "./udevs.png";
import Login from "../Login/login";
import "./header.css";
import { Link } from "react-router-dom";

function Header() {
  return (
    <div>
      <div className="top">
        <Link to="/">
          <img className="logo" src={logo} alt="logo" />
        </Link>
        <Login />
      </div>
      <div className="navbar">
        <ul>
          <li id="isActive">Все потоки</li>
          <li>Разработка</li>
          <li>Администрирование</li>
          <li>Дизайн</li>
          <li>Менеджмент</li>
          <li>Маркетинг</li>
          <li>Научпоп</li>
        </ul>
      </div>
    </div>
  );
}

export default Header;

import ava from "../assets/ava-profile.png";
import style from "./profileInfo.module.css";
import GetResult from "../Create-Post/getResult";

const ProfilePage = () => {
  return (
    <div className={style.info_container}>
      <div className={style.profile_info}>
        <div className={style.img_info}>
          <img src={ava} alt="ava" />
        </div>
        <div className={style.text_info}>
          <div className={style.info_name}>Дилором Алиева</div>
          <div className={style.box}>
            <table cellspacing="0">
              <tr>
                <td>Карьера</td>
                <td>Писатель</td>
              </tr>
              <tr>
                <td>Дата рождения</td>
                <td>2 ноября, 1974 ( 46 лет)</td>
              </tr>
              <tr>
                <td>Место рождения</td>
                <td>Черняховск, СССР (Россия)</td>
              </tr>
            </table>
          </div>
        </div>
      </div>

      <div className={style.publication}>
        <h3>ПУБЛИКАЦИИ</h3>

        <GetResult />
      </div>
    </div>
  );
};

export default ProfilePage;

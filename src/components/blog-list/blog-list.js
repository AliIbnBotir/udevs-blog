import React, { useEffect } from "react";
import BlogListItem from "../blog-list-item/blog-list-item";
import "./blog-list.css";
import { db } from "../../firebase";
import { collection, getDocs } from "@firebase/firestore";
import { useState } from "react/cjs/react.development";

function BlogList({ posts, setPosts }) {
  const postsCollectionRef = collection(db, "posts");
  const getPostsFromStore = () => {
    getDocs(postsCollectionRef).then((res) => {
      setPosts(res.docs.map((doc) => ({ ...doc.data(), id: doc.id })));
    });
  };

  useEffect(() => {
    getPostsFromStore();
  }, []);
  return (
    <div className="blog-list--container">
      {posts.map((item) => (
        <BlogListItem key={item.id} item={item} />
      ))}
    </div>
  );
}

export default BlogList;

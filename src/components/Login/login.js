import React from "react";
import { useState } from "react";
import ava from "../assets/ava-profile.png";
import style from "./login.module.css";
import { Link } from "react-router-dom";
import "./login.css";
import {
  createUserWithEmailAndPassword,
  onAuthStateChanged,
  signInWithEmailAndPassword,
  signOut,
} from "@firebase/auth";
import { auth } from "../../firebase";
import { FaSignOutAlt } from "react-icons/fa";
// import app from "../../base";

export default function Login() {
  const [IsOpen, setIsOpen] = useState(false); //this is always true

  // reset hidden if type changes

  const [Email, setEmail] = useState("");
  const [Pass, setPass] = useState("");
  const [IsAuthorized, setIsAuthorized] = useState(false);
  const localData = localStorage.getItem("localStrorageData");
  const [user, setUser] = useState({});

  onAuthStateChanged(auth, (currentUser) => {
    setUser(currentUser);
  });

  async function register() {
    try {
      const user = await createUserWithEmailAndPassword(auth, Email, Pass);
      console.log(user);
      window.location.reload(false);
      setIsAuthorized(true);
      localStorage.setItem("localStrorageData", true);
    } catch (error) {
      console.log(error.message);
      alert("there is problem");
    }
  }
  async function logIn() {
    try {
      const user = await signInWithEmailAndPassword(auth, Email, Pass);
      console.log(user);
      window.location.reload(false);
      setIsAuthorized(true);
      localStorage.setItem("localStrorageData", true);
    } catch (error) {
      console.log(error.message);
      alert("there is problem");
    }
  }
  async function logOut() {
    localStorage.clear();
    setIsAuthorized(false);
    await signOut(auth);
  }
  return (
    <div className="login-container">
      {localData && (
        <div style={{ display: "inline" }}>
          <button>{user.email}</button>
          <div className={style.dropdown}>
            <img src={ava} className={style.ava} alt="" />
            <div className={style.dropdown_content}>
              <Link to="/publish">Написать публикацию</Link>
              <Link to="/">Избранные</Link>
              <Link to="/">Выйти</Link>
            </div>
          </div>
          <button
            onClick={() => {
              logOut();
            }}
          >
            <FaSignOutAlt />
          </button>
        </div>
      )}
      {!IsAuthorized && !localData && (
        <button
          onClick={() => {
            setIsOpen(true);
          }}
          className="btn-login"
        >
          Войти
        </button>
      )}
      <div className="modal">
        <h1 className="modal__title">Login</h1>
      </div>
      <div>
        <div className={`modal ${IsOpen ? "visible" : ""}`}>
          <h3 className="modal__title">Вход на udevs news</h3>

          <div className="email-password">
            <input
              onChange={(e) => {
                setEmail(e.target.value);
              }}
              className="input email"
              type="text"
              placeholder="Email"
            />
            <input
              onChange={(e) => {
                setPass(e.target.value);
              }}
              className="input pass"
              type="password"
              placeholder="Пароль"
            />
          </div>

          <button
            onClick={() => {
              logIn();
            }}
            className="btn_login"
          >
            Войти
          </button>
          <button
            className="btn_login register-btn"
            onClick={() => {
              register();
            }}
          >
            Sing up
          </button>
        </div>
        {IsOpen && (
          <div
            onClick={() => {
              setIsOpen(false);
            }}
            className="overlay"
          />
        )}
      </div>
    </div>
  );
}

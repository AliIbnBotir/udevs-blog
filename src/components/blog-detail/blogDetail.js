import { Link, useParams } from "react-router-dom";
import Author from "./author.png";
import Eye from "./eye.svg";
import "./blogDetail.css";
import { BsFillBookmarkFill } from "react-icons/bs";
import { useEffect } from "react";
import { db } from "../../firebase";
import { collection, getDocs } from "@firebase/firestore";
const postsCollectionRef = collection(db, "posts");

// import BlogListItem from "../blog-list-item/blog-list-item";
const BlogDetail = ({ choosenPost, setChoosenPost }) => {
  const { id } = useParams();
  const getPostsFromStore = (id) => {
    getDocs(postsCollectionRef).then((res) => {
      res.docs.forEach((doc) => {
        if (id === doc.id) {
          setChoosenPost(doc.data());
        }
      });
    });
  };
  useEffect(() => {
    getPostsFromStore(id);
  }, []);
  return (
    <div className="blog-details">
      <section className="blog-details__author">
        <Link to="/profile">
          <img src={Author} alt="" className="author-img" />

          <h1 className="author">Dilorom Aliyeva</h1>
        </Link>
        <div className="buttons">
          <button className="follow-btn">Follow</button>
          <button className="bookmarks-btn">
            <BsFillBookmarkFill />
          </button>
        </div>
      </section>
      <article className="blog-details__article">
        <img src={choosenPost.image} alt="" className="article-image" />
        <p className="author">Фото: Dilorom Alieva</p>
        <p className="secondary-info">
          18:26 11.01.2021 |
          <span>
            <img src={Eye} alt="" />
          </span>{" "}
          365
        </p>
        <h1 className="article-title">{choosenPost.title}</h1>
        <p className="article-body">{choosenPost.description}</p>
      </article>
      {/* <aside className="blog-details__aside">
        {posts.map((item) => (
          // console.log(item),
          <Link key={item.id} to={`/${item.id}`}>
            <BlogListItem
              title={item.title}
              body={item.body}
              onClick={() => {
                getPost(item.id);
              }}
            />
          </Link>
        ))}
      </aside> */}
    </div>
  );
};

export default BlogDetail;

import style from "./createPost.module.css";
import React from "react";
import { useState } from "react";
import { db, storage } from "../../firebase";
import { doc, setDoc } from "firebase/firestore";
import { Link } from "react-router-dom";

const CreatePost = () => {
  console.log("Create Post component loaded");
  const [createdTitle, setCreatedTitle] = useState("");
  const [createdDescription, setCreatedDescription] = useState("");
  const [choosenImage, setChoosenImage] = useState(
    "https://firebasestorage.googleapis.com/v0/b/udevs-915a0.appspot.com/o/_120995731_watson_mural.jpg?alt=media&token=9d4dfc1d-4a3f-47a0-ad04-424ab5fb2f44"
  );
  function generateDate() {
    let newDate = new Date();
    let date = newDate.getDate();
    let month = newDate.getMonth() + 1;
    let year = newDate.getFullYear();
    let time = newDate.getTime();
    return `${year}${month < 10 ? `0${month}` : `${month}`}${date}${time}`;
  }
  // function imageHandle(e) {
  //   if (e.target.files[0]) {
  //     setImage(e.target.files[0]);
  //   }
  // }

  async function createPostHander() {
    await setDoc(doc(db, "posts", generateDate()), {
      title: createdTitle,
      description: createdDescription,
      image: choosenImage,
      id: generateDate(),
      created: true,
    });
    console.log(createdTitle);
    console.log(createdDescription);
  }
  return (
    <div>
      <div className={style.publish}>
        <h2 className={style.h2}> Настройки публикации </h2>
        <div className={style.title_header}>Название</div>
        <input
          placeholder="Text Input"
          className={style.publish_title}
          // value={this.state.title}
          onChange={(e) => setCreatedTitle(e.target.value)}
        />
        <div className={style.text_header}>Описание</div>
        <textarea
          placeholder="Write your text here"
          className={style.publish_text}
          rows="10"
          // value={this.state.text}
          onChange={(e) => {
            setCreatedDescription(e.target.value);
          }}
        />
        {/* <div className={style.text_header}>Image</div>
        <input type="file" onChange={imageHandle} /> */}
        <Link to="/profile">
          <button onClick={createPostHander}>Publish</button>
        </Link>
      </div>
    </div>
  );
};

export default CreatePost;

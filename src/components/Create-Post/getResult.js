import React, { useEffect } from "react";
import instance from "./instance";
import ResultDisplay from "./resultDisplay.js";

import { db } from "../../firebase";
import { collection, getDocs } from "@firebase/firestore";
import { useState } from "react";

const GetResult = () => {
  const [profilePosts, setProfilePosts] = useState([]);
  const postsCollectionRef = collection(db, "posts");
  const getPostsFromStore = () => {
    getDocs(postsCollectionRef).then((res) => {
      setProfilePosts(res.docs.map((doc) => ({ ...doc.data(), id: doc.id })));
    });
  };
  useEffect(() => {
    getPostsFromStore();
  }, []);
  return (
    <div>
      {profilePosts.map((item) => {
        if (item.created) {
          <ResultDisplay
            image={item.image}
            title={item.title}
            text={item.description}
            key={item.id}
            // image={item.image}
          />;
        }
      })}
    </div>
  );
};

export default GetResult;

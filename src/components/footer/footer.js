import React from "react";
import logo from "./logo.png";
import "./footer.css";

function Footer() {
  return (
    <div>
      <div className="footer-container">
        <div className="container">
          <div className="footer-title">
            <img src={logo} alt="logo" />
            <p className="footer-info">
              Помощник в публикации статей, журналов.
              <br />
              Список популярных международных конференций.
              <br />
              Всё для студентов и преподавателей.
              <br />
            </p>
          </div>
          <div>
            <ul>
              <li id="th_footer">Ресурсы</li>
              <li>Статьи</li>
              <li>Журналы</li>
              <li>Газеты</li>
              <li>Диплом</li>
            </ul>
          </div>
          <div>
            <ul>
              <li id="th_footer">О нас</li>
              <li>Контакты</li>
              <li>Помощь</li>
              <li>Заявки</li>
              <li>Политика</li>
            </ul>
          </div>
          <div>
            <ul>
              <li id="th_footer">Помощь</li>
              <li>Часто задаваемые вопросы</li>
            </ul>
          </div>
        </div>
        <p className="copyright">
          Copyright © 2020. LogoIpsum. All rights reserved.
        </p>
      </div>
    </div>
  );
}

export default Footer;

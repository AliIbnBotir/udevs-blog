import React, { useState } from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import "./App.css";
import Header from "./components/header/header";
import BlogList from "./components/blog-list/blog-list";
import Footer from "./components/footer/footer";
import BlogDetail from "./components/blog-detail/blogDetail";
import ProfilePage from "./components/profile-page/profile-page";

import CreatePost from "./components/Create-Post/Create-Post";

function App() {
  const [posts, setPosts] = useState([]);
  const [choosenPost, setChoosenPost] = useState("");

  return (
    <Router>
      <div className="App">
        <div className="container">
          <Header />
          <Routes>
            <Route
              exact
              path="/"
              element={<BlogList posts={posts} setPosts={setPosts} />}
            ></Route>
            <Route
              exact
              path="/:id"
              element={
                <BlogDetail
                  choosenPost={choosenPost}
                  setChoosenPost={setChoosenPost}
                />
              }
            ></Route>
            <Route path="/profile" element={<ProfilePage />}></Route>
            <Route path="/publish" element={<CreatePost />}></Route>
          </Routes>
        </div>
        <Footer />
      </div>
    </Router>
  );
}

export default App;
